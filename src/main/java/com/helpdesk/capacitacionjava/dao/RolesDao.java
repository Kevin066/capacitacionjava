/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.helpdesk.capacitacionjava.dao;


import com.helpdesk.capacitacionjava.modelos.Roles;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author David Ramirez
 */
@Local

public interface RolesDao {
    public List<Roles> listaRoles();
    public Roles obtenerRol(Integer idRol);

}
