/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.helpdesk.capacitacionjava.dao;

import com.helpdesk.capacitacionjava.modelos.Usuarios;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author David Ramirez
 */
@Local
public interface UsuarioDao {
    public List<Usuarios> listaUsuarios();
    public void guardarUsuario(Usuarios usr);
    public void editarUsuario(Usuarios usr);
    public Usuarios buscarUsuario(Integer usur_id);
    public void eliminarUsuario(Usuarios usuario);
}
