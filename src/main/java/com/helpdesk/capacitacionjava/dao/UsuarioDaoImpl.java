/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.helpdesk.capacitacionjava.dao;

import com.helpdesk.capacitacionjava.modelos.Usuarios;
import com.helpdesk.capacitacionjava.util.NewHibernateUtil;
import java.util.List;
import javax.ejb.Stateless;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author David Ramirez
 */
@Stateless
public class UsuarioDaoImpl implements UsuarioDao{

    @Override
    public List<Usuarios> listaUsuarios() {
       Session session = NewHibernateUtil.getSessionFactory().getCurrentSession();
        Transaction transaction = null;
        transaction = session.beginTransaction();
        List<Usuarios> listaUsuario = session.createQuery("From Usuarios usr "
                + "inner join fetch usr.roles rl").list();
        transaction.commit();
        return listaUsuario;
    }

    @Override
    public void guardarUsuario(Usuarios usuario) {
      Session session = NewHibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        transaction = session.beginTransaction();
        session.persist(usuario);
        transaction.commit();
    }

    @Override
    public void editarUsuario(Usuarios usuario) {
       Session session = NewHibernateUtil.buildSessionFactoryObj().getCurrentSession();
       Transaction transaction = null;
       transaction = session.beginTransaction();
       session.saveOrUpdate(usuario);
       transaction.commit();
    }

    @Override
    public Usuarios buscarUsuario(Integer usur_id) {
      Session session = NewHibernateUtil.getSessionFactory().getCurrentSession();
      Transaction transaction = null;
      transaction = session.beginTransaction();
      Usuarios usuariosN = (Usuarios) session.createQuery("FROM Usuarios usr "
              + "INNER JOIN FETCH usr.roles rl "
              + "WHERE usr.idUsuario=:idUsuario ").setParameter("idUsuario", usur_id).uniqueResult();
      transaction.commit();
      return usuariosN;
    }

    @Override
    public void eliminarUsuario(Usuarios usuario) {
      Session session = NewHibernateUtil.getSessionFactory().getCurrentSession();
      Transaction transaction = null;
      transaction = session.beginTransaction();
      Usuarios usuariosN = (Usuarios) session.createQuery("FROM Usuarios usr "
              + "INNER JOIN FETCH usr.roles rl "
              + "WHERE usr.idUsuario=:idUsuario ").setParameter("idUsuario", usuario.getIdUsuario()).uniqueResult();
      session.delete(usuariosN);
      transaction.commit();
    }
    
}
