/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.helpdesk.capacitacionjava.dto;

/**
 *
 * @author Programacion
 */
public class ReporteDiarioDTO {
    private Double unidades;
    private String nombre;
    private Double total;
    
    public ReporteDiarioDTO(){}

    public ReporteDiarioDTO(Double unidades, String nombre, Double total) {
        this.unidades = unidades;
        this.nombre = nombre;
        this.total = total;
    }

    public Double getUnidades() {
        return unidades;
    }

    public String getNombre() {
        return nombre;
    }

    public Double getTotal() {
        return total;
    }
    
}
