/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.helpdesk.capacitacionjava.servicios;

import com.helpdesk.capacitacionjava.modelos.Bitacora;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author David Ramirez
 */
@Local
public interface BitacoraService{
    public List<Bitacora> listaBitacora();
    public void guardarBitacora(Bitacora bitacora);
    public void actualizarBitacora(Bitacora bitacora);
    public Bitacora buscarBitacora(Integer bita_id);
    public void eleminarBitacora(Bitacora bitacora);

}
